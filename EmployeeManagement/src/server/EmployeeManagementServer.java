/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import common.EmployeeManagement;
import common.EmployeeManagementImpl;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 *
 * @author
 */
public class EmployeeManagementServer {

    public static void main(String[] args) {
        String serverName = "rmi://localhost:41297/Employee";
        EmployeeManagement server = null;
        try {
            LocateRegistry.createRegistry(41297);
            server = new EmployeeManagementImpl();
            Naming.bind(serverName, server);
            System.out.println("Service" + serverName + " is running");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
